﻿Option Explicit On
Option Strict On

<System.Serializable()> Public Structure Song
    Dim mTitle As String
    Dim mArtist As String
    Dim mAlbum As String
    Dim mGenre As String
    Dim mDuration As String
    Dim mType As String
    Dim mPath As String
    Dim mComments As String

    Sub New(ByVal path As String, ByVal media As WMPLib.IWMPMedia3)
        Me.mTitle = media.name
        Me.mArtist = media.getItemInfo("Artist")
        Me.mAlbum = media.getItemInfo("Album")
        Me.mGenre = media.getItemInfo("Genre")
        Me.mDuration = media.durationString
        Me.mType = path.Substring(path.LastIndexOf(".") + 1).ToUpper
        Me.mPath = path
        Me.mComments = media.getItemInfo("Comment")
    End Sub

    Public Function getFields() As String()
        Dim fields() As String = {Me.mTitle, Me.mArtist, Me.mAlbum, Me.mGenre, Me.mDuration, Me.mType, Me.mPath, Me.mComments}
        Return fields
    End Function

End Structure
