﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainWindow
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainWindow))
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.tsAddSong = New System.Windows.Forms.ToolStripButton()
        Me.tsDeleteSong = New System.Windows.Forms.ToolStripButton()
        Me.tsClearPlaylist = New System.Windows.Forms.ToolStripButton()
        Me.tsHelpButton = New System.Windows.Forms.ToolStripButton()
        Me.libraryTable = New System.Windows.Forms.ListView()
        Me.Title = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Artist = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Album = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Genre = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Duration = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Type = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Path = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Comments = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.mediaPlayer = New AxWMPLib.AxWindowsMediaPlayer()
        Me.playList = New System.Windows.Forms.ListView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.ToolStrip1.SuspendLayout()
        CType(Me.mediaPlayer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsAddSong, Me.tsDeleteSong, Me.tsClearPlaylist, Me.tsHelpButton})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1278, 58)
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'tsAddSong
        '
        Me.tsAddSong.Image = CType(resources.GetObject("tsAddSong.Image"), System.Drawing.Image)
        Me.tsAddSong.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsAddSong.Name = "tsAddSong"
        Me.tsAddSong.Size = New System.Drawing.Size(129, 55)
        Me.tsAddSong.Text = "Add Song"
        Me.tsAddSong.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.tsAddSong.ToolTipText = "Add Song to Library"
        '
        'tsDeleteSong
        '
        Me.tsDeleteSong.Image = CType(resources.GetObject("tsDeleteSong.Image"), System.Drawing.Image)
        Me.tsDeleteSong.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsDeleteSong.Name = "tsDeleteSong"
        Me.tsDeleteSong.Size = New System.Drawing.Size(154, 55)
        Me.tsDeleteSong.Text = "Delete Song"
        Me.tsDeleteSong.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.tsDeleteSong.ToolTipText = "Delete Song from Library"
        '
        'tsClearPlaylist
        '
        Me.tsClearPlaylist.Image = CType(resources.GetObject("tsClearPlaylist.Image"), System.Drawing.Image)
        Me.tsClearPlaylist.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsClearPlaylist.Name = "tsClearPlaylist"
        Me.tsClearPlaylist.Size = New System.Drawing.Size(158, 55)
        Me.tsClearPlaylist.Text = "Clear Playlist"
        Me.tsClearPlaylist.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'tsHelpButton
        '
        Me.tsHelpButton.Image = CType(resources.GetObject("tsHelpButton.Image"), System.Drawing.Image)
        Me.tsHelpButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsHelpButton.Name = "tsHelpButton"
        Me.tsHelpButton.Size = New System.Drawing.Size(71, 55)
        Me.tsHelpButton.Text = "Help"
        Me.tsHelpButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'libraryTable
        '
        Me.libraryTable.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.libraryTable.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Title, Me.Artist, Me.Album, Me.Genre, Me.Duration, Me.Type, Me.Path, Me.Comments})
        Me.libraryTable.FullRowSelect = True
        Me.libraryTable.GridLines = True
        Me.libraryTable.Location = New System.Drawing.Point(47, 551)
        Me.libraryTable.MultiSelect = False
        Me.libraryTable.Name = "libraryTable"
        Me.libraryTable.Size = New System.Drawing.Size(1180, 284)
        Me.libraryTable.TabIndex = 2
        Me.libraryTable.UseCompatibleStateImageBehavior = False
        Me.libraryTable.View = System.Windows.Forms.View.Details
        '
        'Title
        '
        Me.Title.Text = "Title"
        Me.Title.Width = 100
        '
        'Artist
        '
        Me.Artist.Text = "Artist"
        Me.Artist.Width = 100
        '
        'Album
        '
        Me.Album.Text = "Album"
        Me.Album.Width = 100
        '
        'Genre
        '
        Me.Genre.Text = "Genre"
        '
        'Duration
        '
        Me.Duration.Text = "Duration"
        '
        'Type
        '
        Me.Type.Text = "Type"
        '
        'Path
        '
        Me.Path.Text = "Path"
        Me.Path.Width = 100
        '
        'Comments
        '
        Me.Comments.Text = "Comments"
        Me.Comments.Width = 100
        '
        'mediaPlayer
        '
        Me.mediaPlayer.Enabled = True
        Me.mediaPlayer.Location = New System.Drawing.Point(24, 76)
        Me.mediaPlayer.Name = "mediaPlayer"
        Me.mediaPlayer.OcxState = CType(resources.GetObject("mediaPlayer.OcxState"), System.Windows.Forms.AxHost.State)
        Me.mediaPlayer.Size = New System.Drawing.Size(295, 176)
        Me.mediaPlayer.TabIndex = 3
        '
        'playList
        '
        Me.playList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.playList.Location = New System.Drawing.Point(708, 146)
        Me.playList.Name = "playList"
        Me.playList.Size = New System.Drawing.Size(519, 337)
        Me.playList.TabIndex = 4
        Me.playList.UseCompatibleStateImageBehavior = False
        Me.playList.View = System.Windows.Forms.View.List
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Location = New System.Drawing.Point(682, 103)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(572, 406)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Playlist"
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Location = New System.Drawing.Point(25, 515)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1229, 348)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Library"
        '
        'GroupBox3
        '
        Me.GroupBox3.Location = New System.Drawing.Point(25, 103)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(641, 405)
        Me.GroupBox3.TabIndex = 7
        Me.GroupBox3.TabStop = False
        '
        'MainWindow
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1278, 1031)
        Me.Controls.Add(Me.playList)
        Me.Controls.Add(Me.mediaPlayer)
        Me.Controls.Add(Me.libraryTable)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox3)
        Me.MinimumSize = New System.Drawing.Size(1300, 1100)
        Me.Name = "MainWindow"
        Me.Text = "Jukebox"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.mediaPlayer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents tsAddSong As System.Windows.Forms.ToolStripButton
    Friend WithEvents libraryTable As System.Windows.Forms.ListView
    Friend WithEvents Title As System.Windows.Forms.ColumnHeader
    Friend WithEvents Artist As System.Windows.Forms.ColumnHeader
    Friend WithEvents Path As System.Windows.Forms.ColumnHeader
    Friend WithEvents mediaPlayer As AxWMPLib.AxWindowsMediaPlayer
    Friend WithEvents playList As System.Windows.Forms.ListView
    Friend WithEvents Album As System.Windows.Forms.ColumnHeader
    Friend WithEvents Duration As System.Windows.Forms.ColumnHeader
    Friend WithEvents tsDeleteSong As System.Windows.Forms.ToolStripButton
    Friend WithEvents Comments As System.Windows.Forms.ColumnHeader
    Friend WithEvents Genre As System.Windows.Forms.ColumnHeader
    Friend WithEvents Type As System.Windows.Forms.ColumnHeader
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents tsHelpButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsClearPlaylist As System.Windows.Forms.ToolStripButton

End Class
