﻿Option Explicit On
Option Strict On

Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary

Public Class MainWindow

    Private LIBRARY_FILE As String = Application.StartupPath & "/SongLibrary"
    Private songLibrary As List(Of Song)
    Private mediaPlayerPlaylist As WMPLib.IWMPPlaylist

    ''' <summary>
    ''' Persists the current song library to a file.
    ''' </summary>
    Private Sub saveLibrary()
        Dim fileStream As Stream = Nothing
        Try
            fileStream = File.OpenWrite(LIBRARY_FILE)
            Dim serializer As New BinaryFormatter()
            serializer.Serialize(fileStream, songLibrary)
        Catch ex As Exception
            MsgBox("Unable to save song library.", MsgBoxStyle.Critical)
        Finally
            If Not IsNothing(fileStream) Then
                fileStream.Close()
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Loads the current song library from a file.
    ''' </summary>
    Private Sub loadLibrary()
        ' Create the file if it doesn't exist.
        If Not System.IO.File.Exists(LIBRARY_FILE) Then
            System.IO.File.Create(LIBRARY_FILE).Dispose()
        End If

        ' Load the file into our library.
        Dim fileStream As Stream = Nothing
        Try
            fileStream = File.OpenRead(LIBRARY_FILE)
            songLibrary = CType(New BinaryFormatter().Deserialize(fileStream), List(Of Song))
        Catch ex As Exception
            songLibrary = New List(Of Song)()
        Finally
            If Not IsNothing(fileStream) Then
                fileStream.Close()
            End If
        End Try
        populateLibraryTable()
    End Sub

    ''' <summary>
    ''' Populates the current library table ListView.
    ''' </summary>
    Private Sub populateLibraryTable()
        Me.libraryTable.Items.Clear()
        Dim i As Integer = 0
        While i < songLibrary.Count
            Dim song As Song = songLibrary.Item(i)
            Me.libraryTable.Items.Add(New ListViewItem(song.getFields()))
            i += 1
        End While
    End Sub

    ''' <summary>
    ''' Opens a dialog to get a song file from the user.
    ''' </summary>
    ''' <returns>The file selected by the user or nothing.</returns>
    Private Function getSong() As String
        Dim findFileDialog As New OpenFileDialog
        Dim result As DialogResult

        With findFileDialog
            .Title = "Add Song"
            .InitialDirectory = Directory.GetCurrentDirectory & "/../../../Assets/Songs"
            .FileName = ""

            .Filter = "TextFiles (*.wma)|*.wma|(*.mp3)|*.mp3"
            result = .ShowDialog
        End With

        If result <> DialogResult.Cancel Then
            Return findFileDialog.FileName
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Adds a song to the library given its path.
    ''' </summary>
    ''' <param name="songPath">Path to the new song file.</param>
    Private Sub addSong(ByVal songPath As String)
        If songPath <> Nothing Then
            ' See if we already have the song.
            Dim exists As Boolean = False
            For Each song As Song In songLibrary
                If song.mPath = songPath Then
                    exists = True
                    Exit For
                End If
            Next

            ' Add it if we don't have it.
            If exists Then
                MsgBox("Song already exists in library.", MsgBoxStyle.Information)
            Else
                Dim mediaFile As WMPLib.IWMPMedia3 = CType(mediaPlayer.newMedia(songPath), WMPLib.IWMPMedia3)
                songLibrary.Add(New Song(songPath, mediaFile))
                saveLibrary()
                populateLibraryTable()
            End If
        End If
    End Sub

    ''' <summary>
    ''' Deletes a song from the current library.
    ''' </summary>
    ''' <param name="song">Song to be deleted.</param>
    Private Sub deleteSong(ByRef song As Song)
        songLibrary.Remove(song)
        saveLibrary()
        populateLibraryTable()
    End Sub

    ''' <summary>
    ''' Adds a song to the current playlist.
    ''' </summary>
    ''' <param name="song">The song to be added.</param>
    Private Sub addToPlaylist(ByVal song As Song)
        'Add it to the current playlist.
        playList.Items.Add(song.mTitle)
        Dim mediaFile As WMPLib.IWMPMedia3 = CType(mediaPlayer.newMedia(song.mPath), WMPLib.IWMPMedia3)
        mediaPlayerPlaylist.appendItem(mediaFile)

        ' Start the playlist if its not currently playing.
        If (mediaPlayer.playState <> WMPLib.WMPPlayState.wmppsPlaying) Then
            mediaPlayer.currentPlaylist = mediaPlayerPlaylist
        End If
    End Sub

    ' --------------------------------------------------------------------------------
    ' Event Handlers
    ' --------------------------------------------------------------------------------

    ''' <summary>
    ''' Event Handler for when the main form loads.
    ''' </summary>
    Private Sub MainWindow_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadLibrary()

        ' Create a new playlist.
        mediaPlayerPlaylist = mediaPlayer.playlistCollection.newPlaylist("Current Playlist")
    End Sub

    ''' <summary>
    ''' Event Handler for the addSong button.
    ''' </summary>
    Private Sub tsAddSong_Click(sender As Object, e As EventArgs) Handles tsAddSong.Click
        addSong(getSong())
    End Sub

    ''' <summary>
    ''' Event Handler for the deleteSong button.
    ''' </summary>
    Private Sub tsDeleteSong_Click(sender As Object, e As EventArgs) Handles tsDeleteSong.Click
        If Not IsNothing(libraryTable.FocusedItem) Then
            Dim song As Song = songLibrary.Item(libraryTable.FocusedItem.Index)
            deleteSong(song)
        End If
    End Sub

    ''' <summary>
    ''' Event Handler for clicking the ListView columns.
    ''' </summary>
    Private Sub libraryTable_ColumnClick(sender As Object, e As System.Windows.Forms.ColumnClickEventArgs) Handles libraryTable.ColumnClick
        Select Case e.Column
            Case 0
                songLibrary.Sort(Function(x, y) x.mTitle.CompareTo(y.mTitle))
            Case 1
                songLibrary.Sort(Function(x, y) x.mArtist.CompareTo(y.mArtist))
            Case 2
                songLibrary.Sort(Function(x, y) x.mAlbum.CompareTo(y.mAlbum))
            Case 3
                songLibrary.Sort(Function(x, y) x.mGenre.CompareTo(y.mGenre))
            Case 4
                songLibrary.Sort(Function(x, y) x.mDuration.CompareTo(y.mDuration))
            Case 5
                songLibrary.Sort(Function(x, y) x.mType.CompareTo(y.mType))
            Case 6
                songLibrary.Sort(Function(x, y) x.mPath.CompareTo(y.mPath))
            Case 7
                songLibrary.Sort(Function(x, y) x.mComments.CompareTo(y.mComments))
        End Select
        populateLibraryTable()
    End Sub

    ''' <summary>
    ''' Event Handler for double-clicking the library songs.
    ''' </summary>
    Private Sub libraryTable_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles libraryTable.DoubleClick
        addToPlaylist(songLibrary.Item(libraryTable.FocusedItem.Index))
    End Sub

    ''' <summary>
    ''' Event Handler for double-clicking the playlist songs.
    ''' </summary>
    Private Sub playList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles playList.DoubleClick
        mediaPlayer.Ctlcontrols.playItem(mediaPlayerPlaylist.Item(playList.FocusedItem.Index))
    End Sub

    ''' <summary>
    ''' Event Handler for the clearPlaylist button.
    ''' </summary>
    Private Sub tsClearPlaylist_Click(sender As Object, e As EventArgs) Handles tsClearPlaylist.Click
        mediaPlayer.Ctlcontrols.stop()
        mediaPlayerPlaylist.clear()
        playList.Items.Clear()
    End Sub

    ''' <summary>
    ''' Event Handler for the help button.
    ''' </summary>
    Private Sub tsHelpButton_Click(sender As Object, e As EventArgs) Handles tsHelpButton.Click
        Dim help As HelpWindow = New HelpWindow()
        help.Show()
    End Sub

End Class
